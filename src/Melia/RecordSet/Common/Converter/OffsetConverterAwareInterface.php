<?php

namespace Melia\RecordSet\Common\Converter;

/**
 * Interface of OffsetConverterAwareInterface
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface OffsetConverterAwareInterface {

    /**
     * Get offset converter
     *
     * @return OffsetConverter
     */
    public function getOffsetConverter();
}
<?php

namespace Melia\RecordSet\Common\Converter;

/**
 * Interface of OffsetConverter
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface OffsetConverter {

    /**
     * Convert offset
     *
     * @param mixed $offset            
     * @return mixed
     */
    public function offsetConvert($offset);
}
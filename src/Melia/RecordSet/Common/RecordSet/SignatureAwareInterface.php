<?php

namespace Melia\RecordSet\Common\RecordSet;

/**
 * Interface of SignatureAwareInterface
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface SignatureAwareInterface {

    /**
     * Retrieve the signature
     *
     * @return string
     */
    public function getSignature();
}
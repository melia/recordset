<?php

namespace Melia\RecordSet\Common\RecordSet;

/**
 * Interface of NamespaceAwareInterface
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface NamespaceAwareInterface {

    /**
     * Get namespace (uuid)
     *
     * @return string
     */
    public function getNamespace();
}
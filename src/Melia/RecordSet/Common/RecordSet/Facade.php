<?php

namespace Melia\RecordSet\Common\RecordSet;

use Melia\RecordNotation\Common\Decoder\DecoderAwareInterface;
use Melia\RecordNotation\Common\Encoder\EncoderAwareInterface;
use Melia\RecordSet\Common\Converter\OffsetConverterAwareInterface;

/**
 * Interface of Facade
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *
 */
interface Facade extends RecordSet, DecoderAwareInterface, EncoderAwareInterface, OffsetConverterAwareInterface, \IteratorAggregate, Namespaces {

    /**
     * Determine wether read operations are supported
     *
     * @return boolean
     */
    public function hasSupportReading();

    /**
     * Determine wether write operations are supported
     *
     * @return boolean
     */
    public function hasSupportWriting();
}
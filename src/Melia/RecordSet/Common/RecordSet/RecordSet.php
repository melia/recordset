<?php

namespace Melia\RecordSet\Common\RecordSet;

use Melia\Uuid\Common\Uuid\UuidAwareInterface;

/**
 * Interface of RecordSet
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface RecordSet extends \ArrayAccess, \Countable, \Traversable, UuidAwareInterface, NamespaceAwareInterface, SignatureAwareInterface {
}
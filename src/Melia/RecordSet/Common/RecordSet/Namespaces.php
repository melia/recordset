<?php

namespace Melia\RecordSet\Common\RecordSet;

/**
 * Interface of Namespaces
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface Namespaces {
    /**
     * Default namespace
     *
     * @var string
     */
    const NAMESPACE_DEFAULT = "bd33a89a-31c5-45be-9790-cd9371262dee";
    /**
     * Namespace for metadata
     *
     * @var string
     */
    const NAMESPACE_METADATA = "8f7e50c6-5cbf-4ce4-bf11-1a3f3c7f1714";
}
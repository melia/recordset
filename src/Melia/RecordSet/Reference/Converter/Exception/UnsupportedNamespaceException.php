<?php

namespace Melia\RecordSet\Reference\Converter\Exception;

/**
 * Implementation of UnsupportedNamespaceException
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class UnsupportedNamespaceException extends Exception {
}
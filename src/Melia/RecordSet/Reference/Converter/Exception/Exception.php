<?php

namespace Melia\RecordNotation\Reference\Converter\Exception;

use Melia\RecordSet\Reference\Exception\Exception as BaseException;

/**
 * Implementation of Exception
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class Exception extends BaseException {
}
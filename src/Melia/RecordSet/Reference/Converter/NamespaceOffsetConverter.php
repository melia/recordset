<?php

namespace Melia\RecordSet\Reference\Converter;

use Melia\Uuid\Reference\Validator\Validator;
use Melia\RecordSet\Reference\RecordSet\Exception\UnsupportedNamespaceException;
use Melia\RecordSet\Common\RecordSet\Namespaces;
use Melia\RecordSet\Common\Converter\OffsetConverter;
use Webpatser\Uuid\Uuid;

/**
 * Implementation of NamespaceOffsetConverter
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *
 */
class NamespaceOffsetConverter implements OffsetConverter {
    /**
     * Namespace
     *
     * @var string
     */
    private $namespace;

    /**
     * Constructor
     *
     * @param string $namespace
     */
    public function __construct($namespace = null) {
        $this->setNamespace($namespace);
    }

    /**
     * Get namespace
     *
     * @return string
     */
    public function getNamespace() {
        return $this->namespace;
    }

    /**
     * Set namespace
     *
     * @param string $namespace
     * @throws UnsupportedNamespaceException
     * @return \Melia\RecordSet\Reference\Converter\NamespaceOffsetConverter
     */
    public function setNamespace($namespace = null) {
        if(null === $namespace) {
            $namespace = Namespaces::NAMESPACE_DEFAULT;
        }
        if(Validator::isSupportedUuid($namespace)) {
            $this->namespace = $namespace;
        } else {
            throw new UnsupportedNamespaceException(sprintf("Unsupported namespace has been detected: %s", var_export($namespace, true)));
        }
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordSet\Common\Converter\OffsetConverter::offsetConvert()
     */
    public function offsetConvert($offset) {
        if(false === Validator::isSupportedUUID($offset)) {
            return (string)Uuid::generate(5, $offset, $this->getNamespace());
        }
        return $offset;
    }
}
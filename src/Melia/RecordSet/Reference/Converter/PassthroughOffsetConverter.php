<?php

namespace Melia\RecordSet\Reference\Converter;

use Melia\RecordSet\Common\Converter\OffsetConverter;

/**
 * Implementation of PassthroughOffsetConverter
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class PassthroughOffsetConverter implements OffsetConverter {

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordSet\Common\Converter\OffsetConverter::offsetConvert()
     */
    public function offsetConvert($offset) {
        return $offset;
    }
}
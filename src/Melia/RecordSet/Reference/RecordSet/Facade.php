<?php

namespace Melia\RecordSet\Reference\RecordSet;

use Melia\RecordSet\Common\RecordSet\Facade as FacadeInterface;
use Melia\RecordNotation\Common\Decoder\Decoder;
use Melia\RecordNotation\Common\Encoder\Encoder;
use Melia\RecordSet\Common\RecordSet\RecordSet;
use Webpatser\Uuid\Uuid;
use Melia\RecordNotation\Reference\Record\Record;
use Melia\RecordSet\Reference\RecordSet\Exception\UnsupportedOperationException;
use Melia\RecordSet\Common\Converter\OffsetConverter;
use Melia\RecordSet\Common\Converter\OffsetConverterAwareInterface;
use Melia\RecordSet\Reference\Converter\PassthroughOffsetConverter;

/**
 * Implementation of Facade
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class Facade implements FacadeInterface, OffsetConverter {
    /**
     * Decoder
     *
     * @var Decoder
     */
    private $decoder;
    /**
     * Encoder
     *
     * @var Encoder
     */
    private $encoder;
    /**
     * Record set
     *
     * @var RecordSet
     */
    private $recordSet;
    /**
     * Offset converter
     *
     * @var OffsetConverter
     */
    private $offsetConverter;

    /**
     * Constructor
     *
     * @param RecordSet $recordSet            
     * @param OffsetConverter $offsetConverter            
     */
    public function __construct(RecordSet $recordSet, OffsetConverter $offsetConverter = null) {
        $this->setRecordSet($recordSet);
        if(null === $offsetConverter) {
            $offsetConverter = new PassthroughOffsetConverter();
        }
        $this->setOffsetConverter($offsetConverter);
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordNotation\Common\Decoder\DecoderAwareInterface::getDecoder()
     */
    public function getDecoder() {
        return $this->decoder;
    }

    /**
     * Set decoder
     *
     * @param Decoder $decoder            
     * @return \Melia\RecordSet\Reference\RecordSet\Facade
     */
    public function setDecoder(Decoder $decoder) {
        $this->decoder = $decoder;
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordSet\Common\RecordSet\Facade::hasSupportReading()
     */
    public function hasSupportReading() {
        return ($this->getDecoder() instanceof Decoder);
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordNotation\Common\Encoder\EncoderAwareInterface::getEncoder()
     */
    public function getEncoder() {
        return $this->encoder;
    }

    /**
     * Set encoder
     *
     * @param Encoder $encoder            
     * @return \Melia\RecordSet\Reference\RecordSet\Facade
     */
    public function setEncoder(Encoder $encoder) {
        $this->encoder = $encoder;
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordSet\Common\RecordSet\Facade::hasSupportWriting()
     */
    public function hasSupportWriting() {
        return ($this->getEncoder() instanceof Encoder);
    }

    /**
     * Retrieve record set
     *
     * @return \Melia\RecordSet\Common\RecordSet\RecordSet
     */
    public function getRecordSet() {
        return $this->recordSet;
    }

    /**
     * Set record set
     *
     * @param RecordSet $recordSet            
     * @return \Melia\RecordSet\Reference\RecordSet\Facade
     */
    protected function setRecordSet(RecordSet $recordSet) {
        $this->recordSet = $recordSet;
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordSet\Common\RecordSet\OffsetConverterAwareInterface::getOffsetConverter()
     */
    public function getOffsetConverter() {
        return $this->offsetConverter;
    }

    /**
     * Set offset converter
     *
     * @param OffsetConverter $offsetConverter            
     * @return \Melia\RecordSet\Reference\RecordSet\Facade
     */
    public function setOffsetConverter(OffsetConverter $offsetConverter) {
        $this->offsetConverter = $offsetConverter;
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordSet\Common\RecordSet\OffsetConverter::offsetConvert()
     */
    public function offsetConvert($offset) {
        // TODO: implement strategy pattern
        $recordSet = $this->getRecordSet();
        if($recordSet instanceof OffsetConverter) {
            return $recordSet->offsetConvert($offset);
        } else if($recordSet instanceof OffsetConverterAwareInterface) {
            return $recordSet->getOffsetConverter()->offsetConvert($offset);
        } else {
            return $this->getOffsetConverter()->offsetConvert($offset);
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see ArrayAccess::offsetExists()
     */
    public function offsetExists($offset) {
        return $this->getRecordSet()->offsetExists($this->offsetConvert($offset));
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see ArrayAccess::offsetSet()
     */
    public function offsetSet($offset, $value) {
        if($this->hasSupportWriting()) {
            $this->getRecordSet()->offsetSet($uuid = $this->offsetConvert($offset), $this->getEncoder()->encode(new Record($value, $uuid)));
        } else {
            throw new UnsupportedOperationException("Writing operation not supported. Review encoder settings!");
        }
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see ArrayAccess::offsetGet()
     */
    public function offsetGet($offset) {
        if($this->hasSupportReading()) {
            return $this->getDecoder()->decode($this->getRecordSet()->offsetGet($this->offsetConvert($offset)))->getData();
        } else {
            throw new UnsupportedOperationException("Reading operation not supported. Review decoder settings!");
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see ArrayAccess::offsetUnset()
     */
    public function offsetUnset($offset) {
        return $this->getRecordSet()->offsetUnset($this->offsetConvert($offset));
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see Countable::count()
     */
    public function count() {
        return $this->getRecordSet()->count();
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see IteratorAggregate::getIterator()
     */
    public function getIterator() {
        return new Iterator($this);
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\Uuid\Common\Uuid\UuidAwareInterface::getUuid()
     */
    public function getUuid() {
        return $this->getRecordSet()->getUuid();
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordSet\Common\RecordSet\NamespaceAwareInterface::getNamespace()
     */
    public function getNamespace() {
        return $this->getRecordSet()->getNamespace();
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordSet\Common\RecordSet\SignatureAwareInterface::getSignature()
     */
    public function getSignature() {
        return $this->getRecordSet()->getSignature();
    }
}
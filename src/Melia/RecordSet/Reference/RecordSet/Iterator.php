<?php

namespace Melia\RecordSet\Reference\RecordSet;

/**
 * Implementation of Iterator
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class Iterator extends \IteratorIterator {
    /**
     * Facade
     *
     * @var Facade
     */
    private $facade;

    /**
     * Constructor
     *
     * @param Facade $facade            
     */
    public function __construct(Facade $facade) {
        $this->setFacade($facade);
        parent::__construct($facade->getRecordSet());
    }

    /**
     * Retrieve facade
     *
     * @return \Melia\RecordSet\Reference\RecordSet\Facade
     */
    private function getFacade() {
        return $this->facade;
    }

    /**
     * Set facade
     *
     * @param Facade $facade            
     * @return \Melia\RecordSet\Reference\RecordSet\Iterator
     */
    private function setFacade(Facade $facade) {
        $this->facade = $facade;
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see IteratorIterator::current()
     */
    public function current() {
        return $this->getFacade()->offsetGet($this->key());
    }
}
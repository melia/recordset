<?php

namespace Melia\RecordSet\Reference\RecordSet\Exception;

/**
 * Implementation of UnsupportedOperationException
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class UnsupportedOperationException extends Exception {
}
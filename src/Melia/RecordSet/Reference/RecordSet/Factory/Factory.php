<?php

namespace Melia\RecordSet\Reference\RecordSet\Factory;

use Melia\RecordNotation\Common\Encoder\Encoder;
use Melia\RecordNotation\Common\Decoder\Decoder;
use Melia\RecordSet\Reference\RecordSet\Facade;
use Melia\RecordSet\Common\RecordSet\RecordSet;

/**
 * Implementation of Factory
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *
 */
class Factory {

    /**
     * Create facade
     *
     * @param RecordSet $recordSet
     * @param Decoder $decoder
     * @param Encoder $encoder
     * @return \Melia\RecordSet\Reference\RecordSet\Facade
     */
    public static function createFacade(RecordSet $recordSet, Decoder $decoder = null, Encoder $encoder = null) {
        $facade = new Facade($recordSet);
        if(null === $decoder) {
            $decoder = new \Melia\RecordNotation\Reference\Decoder\v1\Decoder();
        }
        $facade->setDecoder($decoder);
        if(null === $encoder) {
            $encoder = new \Melia\RecordNotation\Reference\Encoder\v1\Encoder();
        }
        $facade->setEncoder($encoder);
        return $facade;
    }
}
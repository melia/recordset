<?php

namespace Melia\RecordSet\Reference\Exception;

use \InvalidArgumentException as BaseException;

/**
 * Implementation of InvalidArgumentException
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *
 */
class InvalidArgumentException extends BaseException {

    /**
     * Constructor
     *
     * @param mixed $invalidArgument
     * @param integer $code
     * @param \Exception $previous
     */
    public function __construct($invalidArgument, $code = null, $previous = null) {
        parent::__construct(sprintf("Invalid argument has been detected: %s", var_export($invalidArgument, true)), $code, $previous);
    }
}